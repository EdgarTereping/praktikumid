package praktikum7;
import lib.TextIO;

public class GuessingGame {
	public static void main(String[] args) {
		int i, j;
		i = (int)(100*Math.random()) + 1;
		
		while(true){
			System.out.println("Sisesta oma number: ");
			j = TextIO.getlnInt();
			if(i == j){
				System.out.println("Palju õnne, ära arvasid!");
				break;
							}
			if(j < i){
				System.out.println("Liiga väike, proovi uuesti!");
				
			}
			if(j > i){
				System.out.println("Liiga suur, proovi uuesti!");
				
			}
		}
	}
	

}
