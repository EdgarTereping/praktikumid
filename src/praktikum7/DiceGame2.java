package praktikum7;

import lib.TextIO;

public class DiceGame2 {
	public static void main(String[] args) {
		int money, bet, userGuess, actual;
		money = 100;
		while (money > 0) {
			System.out.println("Sul on " + money + "raha.");

			while (true) {
				System.out.println("Panusta kuni väärtuses 25");
				System.out.println("Sisesta panus: ");
				bet = TextIO.getlnInt();

				
				//System.out.println(money + " < " + bet);
				if (money < bet) {
					System.out.println("Sul ei ole nii palju raha, tee uus panus");
					continue;
				}
				if (bet > 0 && bet <= 25) {
					break;
				}
				System.out.println("Panusta korrektne summa!");

			}

			while (true) {

				System.out.println("Sisesta täringu silmade arv, mille peale panustad(1-6)");
				userGuess = TextIO.getlnInt();
				if (userGuess >= 1 && userGuess <= 6)
					break;
				System.out.println("Saad pakkuda ainult väärtusi vahemikus 1 ja 6");

			}

			actual = KullKiri.suvaline(1, 6);

			if (actual != userGuess) {
				System.out.println("Tuli " + actual + ", jäid rahast ilma.");
				money -= bet;

			} else {
				System.out.println("Tuli " + actual + ", saad raha kuuekordselt tagasi.");
				money = money + 6 * bet;
			}

		}
		System.out.println("Raha otsas, mäng läbi.");
	}

}
