package praktikum7;
import lib.TextIO;

public class ReverseWord {
	public static void main(String[] args) {
		String word, word2;
		System.out.println("Sisesta mingi sõna ja ma trükin selle välja tagurpidi: ");
		word = TextIO.getlnString();
		word2 = new StringBuilder(word).reverse().toString();
		System.out.println(word2);
		
	}
}
