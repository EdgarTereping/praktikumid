package praktikum7;

import lib.TextIO;

public class KullKiri {
	public static void main(String[] args) {
		int money, bet, userGuess, actual;
		money = 100;
		while (money > 0) {
			System.out.println("Sul on " + money + "raha.");
			
			while (true) {
				System.out.println("Panusta kuni väärtuses 25");
				System.out.println("Sisesta panus: ");
				bet = TextIO.getlnInt();

				if (bet > 0 && bet <= 25)
					break;
				System.out.println("Panusta korrektne summa!");
				if (money > bet)
					break;
				System.out.println("Sul ei ole nii palju raha!");
			}

			/*
			 * while(true){
			 * 
			 * System.out.println("Kas kull(0) või kiri(1)?"); userGuess =
			 * TextIO.getlnInt(); if(userGuess == 0 || userGuess == 1) break;
			 * System.out.println("Saad pakkuda ainult väärtusi 0 ja 1");
			 * 
			 * }
			 */
			actual = suvaline(0, 1);

			if (actual == 0) {
				System.out.println("Tuli kull, jäid rahast ilma.");
				money -= bet;

			} else {
				System.out.println("tuli kiri, saad raha topelt tagasi.");
				money = money + 2 * bet;
			}
			
			

		}
		System.out.println("Raha otsas, mäng läbi.");

	}

	public static int suvaline(int min, int max) {
		int vahemik = max - min + 1;
		return min + (int) (Math.random() * vahemik);
	}
}
