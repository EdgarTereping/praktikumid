package praktikum11;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class JaavaFX extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.show();
		Pane paigutus = new Pane();
		Scene stseen = new Scene(paigutus, 600, 400);
		primaryStage.setScene(stseen);
		
		Rectangle kast1 = new Rectangle(0, 0, 600, 133);
		kast1.setFill(Color.BLUE);
		Rectangle kast2 = new Rectangle(0, 133, 600, 133);
		kast2.setFill(Color.BLACK);
		Rectangle kast3 = new Rectangle(0, 266, 600, 134);
		kast3.setFill(Color.WHITE);
		paigutus.getChildren().addAll(kast1, kast2, kast3);
		
	}

}
