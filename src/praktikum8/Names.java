package praktikum8;
import java.util.ArrayList;
import java.util.Collections;

import lib.TextIO;

public class Names {
	public static void main(String[] args) {
		ArrayList<String> nimed = new ArrayList<String>();
		System.out.println("Sisesta nimed, lõpetamiseks vajuta tühikut.");
		while(true){
			String nimi = TextIO.getlnString();
			if (nimi.equals(" "))
				break;
			nimed.add(nimi);
		}
		System.out.println("Nimed tähestikulises järjekorras on: ");
		Collections.sort(nimed);
		for (String nimi : nimed) {
		    System.out.println(nimi);
		}
	}

}
