package praktikum8;

public class Rumors {
	public static void main(String[] args) {
		String[] men = new String[] { "Aadu", "Alfred", "Atso" };
		String[] actions = new String[] { "röövib", "peksab", "okupeerib" };
		String[] women = new String[] { "Tiinat", "Taisit", "Teelet" };
		
		String menName = randomElement(men);
		String womenName = randomElement(women);
		String actionsName = randomElement(actions);
		System.out.println(menName + " " + actionsName + " " + womenName + ".");
	}

	public static String randomElement(String[] names) {
		
		int i = (int)(Math.random() * names.length);
		return names[i];
	}

}
