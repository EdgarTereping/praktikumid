package praktikum8;

public class ArrayMax {
	// meetod, mis leiab ühemõõtmelise massiivi maksimumi
	public static int maksimum(int[] massiiv) {
		int k;
		k = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > k) {
				k = massiiv[i];
			}
		}
		return k;
	}

	// meetod, mis leiab maatriksi maksimumi
	public static int maksimum(int[][] maatriks) {
	   // maatriksi rea maksimumi leidmiseks saad siin edukalt kasutada eelmist meetodit
		int k = 0;
		for (int i = 0; i < maatriks.length; i++){
			for (int j = 0; j < maatriks.length; j++){
				if (maatriks[i][j] > k){
					k = maatriks[i][j];
				}
			}
			
		}
		return k;
	}

}
