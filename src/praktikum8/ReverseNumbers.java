package praktikum8;

import lib.TextIO;

public class ReverseNumbers {
	public static void main(String[] args) {
		int[] Numbers = new int[10];
		System.out.println("Sisesta 10 arvu ja ma trükin nad välja vastupidises järjekorras: ");
		for (int i = 0; i < 10; i++) {
			Numbers[i] = TextIO.getlnInt();
		}
		for (int j = 9; j >= 0; j--){
			System.out.println(Numbers[j]);
		}
	}

}
