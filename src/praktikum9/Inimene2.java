package praktikum9;

import lib.TextIO;
import java.util.ArrayList;

public class Inimene2 {

	public static void main(String[] args) {
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		while (true) {
			System.out.println("Lõpetamiseks sisesta tühik. Sisesta nimi: ");
			String nimi = TextIO.getlnString();
			if (nimi.equals(" "))
				break;
			System.out.println("Sisesta vanus: ");
			int vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));

		}

		for (Inimene inimene : inimesed) {
			inimene.tervita();
			System.out.println(inimene);
		}

	}

}
