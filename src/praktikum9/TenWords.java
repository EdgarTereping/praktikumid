package praktikum9;

import lib.TextIO;

public class TenWords {
	public static void main(String[] args) {
		String sonad[];
		sonad = new String[10];
		for (int i = 0; i < 10; i++) {
			sonad[i] = TextIO.getlnString();
		}
		for (int j = 0; j < 10; j++) {
			System.out.println("Sõna on " + sonad[j] + " ja selle sõna pikkus on " + sonad[j].length());
		}

	}

}
