package praktikum9;

import lib.TextIO;

public class EdaspidiTagurpidi {

	public static void main(String[] args) {

		System.out.println("Sisesta mingi sõna!");

		String sona = TextIO.getln();

		String reverse = new StringBuffer(sona).reverse().toString();

		if (sona.equals(reverse)) {
			System.out.println("Sõna on tagurpidi sama!");

		} else {
			System.out.println("Pole tagurpidi sama!");

		}

	}

}
