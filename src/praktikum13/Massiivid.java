package praktikum13;

public class Massiivid {
	public static void main(String[] args) {
		int[] arvud = { 3, 4, 5, 6, 7 };

		int[][] maatriks = { { 1, 1, 1, 1, 1 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };
//		tryki(arvud);
//		System.out.println();
//		tryki(maatriks);
//		System.out.println();
//		tryki(ridadeSummad(maatriks));
//		System.out.println();
//		System.out.println(peaDiagonaaliSumma(maatriks));
//
//		System.out.println(korvalDiagonaaliSumma(maatriks));
//		tryki(ridadeMaksimumid(maatriks));
//		System.out.println();
//		System.out.println(miinimum(maatriks));
		tryki(kahegaJaakMaatriks(5, 5));

	}

	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");

		}
		System.out.println();
		/*
		 * for (int arv: massiiv){ for each tsükkel System.out.print(arv + " ");
		 * }
		 */

	}

	public static void tryki(int[][] maatriks) {
		for (int i = 0; i < maatriks.length; i = i + 1) {
			tryki(maatriks[i]);

		}
	}

	public static int[] ridadeSummad(int[][] maatriks) {

		int sum[] = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				sum[i] += maatriks[i][j];
			}

		}
		return sum;

	}

	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i + j == 4) {
					summa += maatriks[i][j];
				}

			}
		}
		return summa;
	}

	public static int peaDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i == j) {
					summa += maatriks[i][j];
				}
			}
		}

		return summa;
	}

	public static int[] ridadeMaksimumid(int[][] maatriks) {
		int maksimumid[] = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (maksimumid[i] < maatriks[i][j]) {
					maksimumid[i] = maatriks[i][j];
				}

			}
		}

		return maksimumid;
	}

	public static int miinimum(int[][] maatriks) {
		int miinimum = Integer.MAX_VALUE;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (miinimum > maatriks[i][j]){
					miinimum = maatriks[i][j];
				}

			}
		}
		return miinimum;
	}
	public static int[][] kahegaJaakMaatriks(int ridu, int veerge){
		int maatriks [][] = new int [ridu][veerge];
		for( int i = 0; i < ridu; i++){
			for( int j = 0; j < veerge; j++){
				maatriks[i][j] = (i+j)%2; //[(i+j)%2][(i+j)%2];
			}
		}
		return maatriks;
		
	}
	

}
