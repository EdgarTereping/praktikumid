package piano;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class Instructions {

	/**
	 * S�testab juhiste akna kujunduse ja m��tmed.
	 * @param title Akna pealkiri.
	 * @param message Juhised programmi kasutamiseks.
	 */

	public static void display(String title, String message){
		Stage window = new Stage();
		
		window.setTitle(title);
		window.setMinHeight(650);
		window.setMinWidth(610);
		
		
		Label label = new Label();
		label.setText(message);
		label.setFont(Font.font("Serif", FontWeight.BOLD, 35));
		
		Pane layout = new Pane();
		layout.getChildren().addAll(label);
		layout.setId("instpane");
		
		Scene scene = new Scene(layout);
		scene.getStylesheets().addAll(Instructions.class.getResource("css/inststyle.css").toExternalForm());
		
		
		window.setScene(scene);
		window.show();
	}

}
