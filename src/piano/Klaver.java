package piano;

import java.net.URL;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Klaver extends Pane {

	/**
	 * S�testab peaaknal olevad elemendid, maalib graafika, s�testab toimingud
	 * klahvide allavajutamisel ja nende lahtilaskmisel.
	 */
	
	boolean CAPSKeyDown = false;
	boolean AKeyDown = false;
	boolean SKeyDown = false;
	boolean DKeyDown = false;
	boolean FKeyDown = false;
	boolean GKeyDown = false;
	boolean HKeyDown = false;
	boolean JKeyDown = false;
	boolean KKeyDown = false;
	boolean LKeyDown = false;
	boolean SEMICOLONKeyDown = false;
	boolean QUOTEKeyDown = false;
	boolean ENTERKeyDown = false;
	boolean NUMPAD4KeyDown = false;
	boolean NUMPAD5KeyDown = false;
	boolean NUMPAD6KeyDown = false;
	boolean NUMPAD3KeyDown = false;
	boolean QKeyDown = false;
	boolean WKeyDown = false;
	boolean RKeyDown = false;
	boolean TKeyDown = false;
	boolean YKeyDown = false;
	boolean UKeyDown = false;
	boolean IKeyDown = false;
	boolean OKeyDown = false;
	boolean OPEN_BRACKETKeyDown = false;
	boolean CLOSE_BRACKETKeyDown = false;
	boolean BACK_SLASHKeyDown = false;
	boolean NUMPAD8KeyDown = false;
	boolean NUMPAD9KeyDown = false;

	public Klaver() {
		Rectangle[] whiteKeys = new Rectangle[17];
		Rectangle[] blackKeys = new Rectangle[16];
		for (int i = 0; i < 17; i++) {
			Rectangle whiteKey = new Rectangle(100 + i * 60, 300, 55, 200);
			whiteKey.setFill(Color.WHITE);
			whiteKey.setStroke(Color.BLACK);
			whiteKeys[i] = whiteKey;

			getChildren().add(whiteKey);
		}
		for (int j = 0; j < 16; j++) {
			if (j != 2 && j != 6 && j != 9 && j != 13) {
				Rectangle blackKey = new Rectangle(135 + j * 60, 300, 45, 125);
				blackKey.setFill(Color.BLACK);
				blackKey.setStroke(Color.BLACK);
				blackKeys[j] = blackKey;
				getChildren().add(blackKey);

			}

		}
		Label title = new Label("EdgarS�nt");
		title.setFont(Font.font("Serif", FontWeight.BOLD, 90));
		title.relocate(390, 0);

		Button button = new Button("Juhised");
		button.setTranslateX(975);
		button.setTranslateY(20);
		button.setPrefHeight(50);
		button.setPrefWidth(200);

		button.setId("button");
		button.getStylesheets().addAll(this.getClass().getResource("css/buttonstyle.css").toExternalForm());

		button.setOnMouseEntered(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				button.setId("button2");
				button.getStylesheets().addAll(this.getClass().getResource("css/buttonstyle2.css").toExternalForm());
			}
		});
		button.setOnMouseExited(new EventHandler<MouseEvent>() {

			public void handle(MouseEvent event) {
				button.setId("button");

			}
		});

		button.setOnAction(e -> Instructions.display("Juhised", "\n 1) Pane peale inglise klaviatuur \n"
				+ " 2) Aktiveeri NumPad \n\n" + "    Klahvid paiknevad klaviatuuril nii"));

		getChildren().addAll(title, button);

		// VALGED KLAHVID

		setOnKeyPressed(new EventHandler<KeyEvent>() {

			public void playSound(String s) {
				URL path;
				AudioClip ac;

				switch (s) {
				case "CAPS":

					path = getClass().getResource("Helifailid/C2.wav");
					ac = new AudioClip(path.toString());
					if (CAPSKeyDown == false) {
						ac.play();
						CAPSKeyDown = true;
					}
					break;
				case "A":
					path = getClass().getResource("Helifailid/D2.wav");
					ac = new AudioClip(path.toString());
					if (AKeyDown == false) {
						ac.play();
						AKeyDown = true;
					}

					break;
				case "S":
					path = getClass().getResource("Helifailid/E2.wav");
					ac = new AudioClip(path.toString());
					if (SKeyDown == false) {
						ac.play();
						SKeyDown = true;
					}
					break;
				case "D":
					path = getClass().getResource("Helifailid/F2.wav");
					ac = new AudioClip(path.toString());
					if (DKeyDown == false) {
						ac.play();
						DKeyDown = true;
					}
					break;
				case "F":
					path = getClass().getResource("Helifailid/G2.wav");
					ac = new AudioClip(path.toString());
					if (FKeyDown == false) {
						ac.play();
						FKeyDown = true;
					}
					break;
				case "G":
					path = getClass().getResource("Helifailid/A2.wav");
					ac = new AudioClip(path.toString());
					if (GKeyDown == false) {
						ac.play();
						GKeyDown = true;
					}
					break;
				case "H":
					path = getClass().getResource("Helifailid/B2.wav");
					ac = new AudioClip(path.toString());
					if (HKeyDown == false) {
						ac.play();
						HKeyDown = true;
					}
					break;
				case "J":
					path = getClass().getResource("Helifailid/C3.wav");
					ac = new AudioClip(path.toString());
					if (JKeyDown == false) {
						ac.play();
						JKeyDown = true;
					}
					break;
				case "K":
					path = getClass().getResource("Helifailid/D3.wav");
					ac = new AudioClip(path.toString());
					if (KKeyDown == false) {
						ac.play();
						KKeyDown = true;
					}
					break;
				case "L":
					path = getClass().getResource("Helifailid/E3.wav");
					ac = new AudioClip(path.toString());
					if (LKeyDown == false) {
						ac.play();
						LKeyDown = true;
					}
					break;
				case "SEMICOLON":
					path = getClass().getResource("Helifailid/F3.wav");
					ac = new AudioClip(path.toString());
					if (SEMICOLONKeyDown == false) {
						ac.play();
						SEMICOLONKeyDown = true;
					}
					break;
				case "QUOTE":
					path = getClass().getResource("Helifailid/G3.wav");
					ac = new AudioClip(path.toString());
					if (QUOTEKeyDown == false) {
						ac.play();
						QUOTEKeyDown = true;
					}
					break;
				case "ENTER":
					path = getClass().getResource("Helifailid/A3.wav");
					ac = new AudioClip(path.toString());
					if (ENTERKeyDown == false) {
						ac.play();
						ENTERKeyDown = true;
					}
					break;
				case "NUMPAD4":
					path = getClass().getResource("Helifailid/B3.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD4KeyDown == false) {
						ac.play();
						NUMPAD4KeyDown = true;
					}
					break;
				case "NUMPAD5":
					path = getClass().getResource("Helifailid/C4.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD5KeyDown == false) {
						ac.play();
						NUMPAD5KeyDown = true;
					}
					break;
				case "NUMPAD6":
					path = getClass().getResource("Helifailid/D4.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD6KeyDown == false) {
						ac.play();
						NUMPAD6KeyDown = true;
					}
					break;
				case "NUMPAD3":
					path = getClass().getResource("Helifailid/E4.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD3KeyDown == false) {
						ac.play();
						NUMPAD3KeyDown = true;
					}

					break;

				// MUSTAD KLAHVID

				case "Q":

					path = getClass().getResource("Helifailid/Csharp2.wav");
					ac = new AudioClip(path.toString());
					if (QKeyDown == false) {
						ac.play();
						QKeyDown = true;
					}
					break;
				case "W":
					path = getClass().getResource("Helifailid/Dsharp2.wav");
					ac = new AudioClip(path.toString());
					if (WKeyDown == false) {
						ac.play();
						WKeyDown = true;
					}
					break;
				case "R":
					path = getClass().getResource("Helifailid/Fsharp2.wav");
					ac = new AudioClip(path.toString());
					if (RKeyDown == false) {
						ac.play();
						RKeyDown = true;
					}
					break;
				case "T":
					path = getClass().getResource("Helifailid/Gsharp2.wav");
					ac = new AudioClip(path.toString());
					if (TKeyDown == false) {
						ac.play();
						TKeyDown = true;
					}
					break;
				case "Y":
					path = getClass().getResource("Helifailid/Asharp2.wav");
					ac = new AudioClip(path.toString());
					if (YKeyDown == false) {
						ac.play();
						YKeyDown = true;
					}
					break;
				case "I":
					path = getClass().getResource("Helifailid/Csharp3.wav");
					ac = new AudioClip(path.toString());
					if (IKeyDown == false) {
						ac.play();
						IKeyDown = true;
					}
					break;
				case "O":
					path = getClass().getResource("Helifailid/Dsharp3.wav");
					ac = new AudioClip(path.toString());
					if (OKeyDown == false) {
						ac.play();
						OKeyDown = true;
					}
					break;
				case "OPEN_BRACKET":
					path = getClass().getResource("Helifailid/Fsharp3.wav");
					ac = new AudioClip(path.toString());
					if (OPEN_BRACKETKeyDown == false) {
						ac.play();
						OPEN_BRACKETKeyDown = true;
					}
					break;
				case "CLOSE_BRACKET":
					path = getClass().getResource("Helifailid/Gsharp3.wav");
					ac = new AudioClip(path.toString());
					if (CLOSE_BRACKETKeyDown == false) {
						ac.play();
						CLOSE_BRACKETKeyDown = true;
					}
					break;
				case "BACK_SLASH":
					path = getClass().getResource("Helifailid/Asharp3.wav");
					ac = new AudioClip(path.toString());
					if (BACK_SLASHKeyDown == false) {
						ac.play();
						BACK_SLASHKeyDown = true;
					}
					break;
				case "NUMPAD8":
					path = getClass().getResource("Helifailid/Csharp4.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD8KeyDown == false) {
						ac.play();
						NUMPAD8KeyDown = true;
					}
					break;
				case "NUMPAD9":
					path = getClass().getResource("Helifailid/Dsharp4.wav");
					ac = new AudioClip(path.toString());
					if (NUMPAD9KeyDown == false) {
						ac.play();
						NUMPAD9KeyDown = true;
					}

					break;
				default:
					break;

				}

			}

			public void handle(KeyEvent event) {

				switch (event.getCode()) {
				// VALGED KLAHVID
				case CAPS:

					playSound("CAPS");

					whiteKeys[0].setFill(Color.RED);

					break;
				case A:

					playSound("A");
					whiteKeys[1].setFill(Color.RED);

					break;
				case S:

					playSound("S");

					whiteKeys[2].setFill(Color.RED);

					break;
				case D:

					playSound("D");

					whiteKeys[3].setFill(Color.RED);

					break;
				case F:

					playSound("F");

					whiteKeys[4].setFill(Color.RED);

					break;
				case G:

					playSound("G");

					whiteKeys[5].setFill(Color.RED);

					break;
				case H:

					playSound("H");

					whiteKeys[6].setFill(Color.RED);
					break;
				case J:

					playSound("J");

					whiteKeys[7].setFill(Color.RED);

					break;
				case K:

					playSound("K");

					whiteKeys[8].setFill(Color.RED);

					break;
				case L:

					playSound("L");

					whiteKeys[9].setFill(Color.RED);

					break;
				case SEMICOLON:

					playSound("SEMICOLON");

					whiteKeys[10].setFill(Color.RED);

					break;
				case QUOTE:

					playSound("QUOTE");

					whiteKeys[11].setFill(Color.RED);

					break;
				case ENTER:

					playSound("ENTER");

					whiteKeys[12].setFill(Color.RED);

					break;
				case NUMPAD4:

					playSound("NUMPAD4");

					whiteKeys[13].setFill(Color.RED);

					break;
				case NUMPAD5:

					playSound("NUMPAD5");

					whiteKeys[14].setFill(Color.RED);

					break;
				case NUMPAD6:

					playSound("NUMPAD6");

					whiteKeys[15].setFill(Color.RED);

					break;
				case NUMPAD3:

					playSound("NUMPAD3");

					whiteKeys[16].setFill(Color.RED);
					break;

				// MUSTAD KLAHVID
				case Q:

					playSound("Q");

					blackKeys[0].setFill(Color.DARKRED);

					break;
				case W:

					playSound("W");

					blackKeys[1].setFill(Color.DARKRED);

					break;
				case R:

					playSound("R");

					blackKeys[3].setFill(Color.DARKRED);

					break;
				case T:

					playSound("T");

					blackKeys[4].setFill(Color.DARKRED);

					break;
				case Y:

					playSound("Y");

					blackKeys[5].setFill(Color.DARKRED);

					break;
				case I:

					playSound("I");

					blackKeys[7].setFill(Color.DARKRED);

					break;
				case O:

					playSound("O");

					blackKeys[8].setFill(Color.DARKRED);

					break;
				case OPEN_BRACKET:

					playSound("OPEN_BRACKET");

					blackKeys[10].setFill(Color.DARKRED);

					break;
				case CLOSE_BRACKET:

					playSound("CLOSE_BRACKET");

					blackKeys[11].setFill(Color.DARKRED);

					break;
				case BACK_SLASH:

					playSound("BACK_SLASH");

					blackKeys[12].setFill(Color.DARKRED);

					break;
				case NUMPAD8:

					playSound("NUMPAD8");

					blackKeys[14].setFill(Color.DARKRED);

					break;
				case NUMPAD9:

					playSound("NUMPAD9");

					blackKeys[15].setFill(Color.DARKRED);

					break;
				default:
					break;
				}
			}
		});
		setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				
				// VALGED KLAHVID
				
				case CAPS:

					whiteKeys[0].setFill(Color.WHITE);

					CAPSKeyDown = false;

					break;
				case A:

					whiteKeys[1].setFill(Color.WHITE);
					
					AKeyDown = false;

					break;
				case S:

					whiteKeys[2].setFill(Color.WHITE);

					SKeyDown = false;

					break;
				case D:

					whiteKeys[3].setFill(Color.WHITE);

					DKeyDown = false;

					break;
				case F:

					whiteKeys[4].setFill(Color.WHITE);

					FKeyDown = false;

					break;
				case G:

					whiteKeys[5].setFill(Color.WHITE);

					GKeyDown = false;
					
					break;
				case H:

					whiteKeys[6].setFill(Color.WHITE);
					
					HKeyDown = false;

					break;
				case J:

					whiteKeys[7].setFill(Color.WHITE);

					JKeyDown = false;

					break;
				case K:

					whiteKeys[8].setFill(Color.WHITE);

					KKeyDown = false;

					break;
				case L:

					whiteKeys[9].setFill(Color.WHITE);

					LKeyDown = false;

					break;
				case SEMICOLON:

					whiteKeys[10].setFill(Color.WHITE);

					SEMICOLONKeyDown = false;

					break;
				case QUOTE:

					whiteKeys[11].setFill(Color.WHITE);

					QUOTEKeyDown = false;

					break;
				case ENTER:

					whiteKeys[12].setFill(Color.WHITE);

					ENTERKeyDown = false;

					break;
				case NUMPAD4:

					whiteKeys[13].setFill(Color.WHITE);

					NUMPAD4KeyDown = false;

					break;
				case NUMPAD5:

					whiteKeys[14].setFill(Color.WHITE);

					NUMPAD5KeyDown = false;

					break;
				case NUMPAD6:

					whiteKeys[15].setFill(Color.WHITE);

					NUMPAD6KeyDown = false;

					break;
				case NUMPAD3:

					whiteKeys[16].setFill(Color.WHITE);

					NUMPAD3KeyDown = false;

					break;

				// MUSTAD KLAHVID
				case Q:

					blackKeys[0].setFill(Color.BLACK);

					QKeyDown = false;

					break;
				case W:

					blackKeys[1].setFill(Color.BLACK);

					WKeyDown = false;

					break;
				case R:

					blackKeys[3].setFill(Color.BLACK);

					RKeyDown = false;

					break;
				case T:

					blackKeys[4].setFill(Color.BLACK);

					TKeyDown = false;

					break;
				case Y:

					blackKeys[5].setFill(Color.BLACK);

					YKeyDown = false;

					break;
				case I:

					blackKeys[7].setFill(Color.BLACK);

					IKeyDown = false;

					break;
				case O:

					blackKeys[8].setFill(Color.BLACK);

					OKeyDown = false;

					break;
				case OPEN_BRACKET:

					blackKeys[10].setFill(Color.BLACK);

					OPEN_BRACKETKeyDown = false;

					break;
				case CLOSE_BRACKET:

					blackKeys[11].setFill(Color.BLACK);

					CLOSE_BRACKETKeyDown = false;

					break;
				case BACK_SLASH:

					blackKeys[12].setFill(Color.BLACK);

					BACK_SLASHKeyDown = false;

					break;
				case NUMPAD8:

					blackKeys[14].setFill(Color.BLACK);

					NUMPAD8KeyDown = false;

					break;
				case NUMPAD9:

					blackKeys[15].setFill(Color.BLACK);

					NUMPAD9KeyDown = false;

					break;

				default:
					break;

				}
			}
		});

	}

}
