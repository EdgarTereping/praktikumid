package piano;

import javafx.application.Application;
import javafx.scene.Scene;

import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Programm kujutab endast klaverit, mida on v�imalik m�ngida arvutiklahvidel.
 * Klaverihelideks on autori enda h��l.
 * 
 * @author Edgar Tereping
 * @version 1.01 23 Nov 2016
 * 
 */

public class KlaverKaima extends Application {

	/**
	 * S�testab peaakna kujunduse ja m��tmed ning k�ivitab programmi.
	 */

	@Override
	public void start(Stage window) throws Exception {

		Klaver klaver = new Klaver();
		klaver.setId("pane");

		Scene scene = new Scene(klaver, 1200, 600);

		scene.getStylesheets().addAll(this.getClass().getResource("css/style.css").toExternalForm());
		

		window.setScene(scene);
		window.setTitle("EdgarS�nt");
		window.show();
		window.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, e -> System.exit(0));

	}

	public static void main(String[] args) {
		launch(args);
	}

}
