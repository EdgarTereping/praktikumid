package praktikum10;

//import java.util.ArrayList;

public class MassiveMassiiv {

	public static void main(String[] args) {

		int[] massiiv = { 1, 3, 6, 7, 8, 3, 5, 7, 21, 3 };
		int[][] neo = { { 1, 3, 6, 7 }, { 2, 3, 3, 1 }, { 17, 4, 5, 0 }, { -20, 13, 16, 17 } };

		int j, k;
		j = suurim(massiiv);
		k = suurimad(neo);
		/*
		 * ArrayList suurimad = new ArrayList(); for (int i = 0; i < neo.length;
		 * i++){ suurimad.add(suurim(neo[i]));
		 */
	

	System.out.println("Massiivi suurim number on: "+j);System.out.println("Kahemõõtmelise massiivi suurim on: "+k);

	}

	public static int suurim(int[] massiiv) {

		int largest = 0;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > largest) {
				largest = massiiv[i];
			}
		}
		return largest;
	}

	public static int suurimad(int[][] neo) {

		int largest = 0;
		for (int i = 0; i < neo.length; i++) {
			for (int j = 0; j < neo.length; j++) {
				if (neo[i][j] > largest) {
					largest = neo[i][j];
				}
			}
		}
		return largest;
	}
}
