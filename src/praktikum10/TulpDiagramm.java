package praktikum10;

import lib.TextIO;
import java.util.ArrayList;

public class TulpDiagramm {
	public static void main(String[] args) {
		ArrayList<Integer> numbrid = new ArrayList();
		System.out.println("Sisesta numbrid, ära negatiivseid sisesta, lõpetamiseks sisesta 0.");

		while (true) {
			int number = TextIO.getlnInt();
			if (number == 0)
				break;
			if (number < 0) {
				System.out.println("Arv ei tohi negatiivne olla.");
				continue;

			}
			numbrid.add(number);
		}
		for (int i = 0; i < numbrid.size(); i++) {
			int nr = numbrid.get(i);
			System.out.printf("%2d %s\n", nr, genereeriIksid(nr));
		}

	}

	public static String genereeriIksid(int nr) {
		String iksid = "";
		for (int i = 0; i < nr; i++) {
			iksid = iksid + "x";
		}
		return iksid;
	}

}
